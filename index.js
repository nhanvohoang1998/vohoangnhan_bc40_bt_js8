var numberArr = []
var numberArr2 = []
document.querySelector("#themSo").onclick = function (event) {
    event.preventDefault()
    var nhapSoN = document.querySelector("#nhapSoN").value * 1;
    var mangHienTai = document.querySelector("#mangHienTai")
    numberArr.push(nhapSoN)
    document.querySelector("#nhapSoN").value = ""
    mangHienTai.innerHTML = `Array: ${numberArr}`
}

// Bài 1: Tổng số dương 
document.querySelector("#tinhTong").onclick = function () {
    var tongSoDuong = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] >= 0) {
            tongSoDuong += numberArr[i];
        }
    }
    document.querySelector("#tongSoDuong").innerHTML = `Tổng số dương: ${tongSoDuong}`
}

// Bài 2: Đếm số dương 
document.querySelector("#demSo").onclick = function () {
    var countSoDuong = 0;
    for (var i = 0; i < numberArr.length; i++) {
        if (numberArr[i] >= 0) {
            countSoDuong++;
        }
    }
    document.querySelector("#soDuong").innerHTML = `Số dương: ${countSoDuong}`
}

// Bài 3: Tìm số nhỏ nhất 
document.querySelector("#timSoNhoNhat").onclick = function () {
    var min = numberArr[0]
    for (var i = 1; i < numberArr.length; i++) {
        if (numberArr[i] < min) {
            min = numberArr[i];
        }
    }
    document.querySelector("#soNhoNhat").innerHTML = `Số dương: ${min}`
}

// Bài 4: Tìm số dương nhỏ nhất 
document.querySelector("#timSoDuongNhoNhat").onclick = function () {
    var soDuongArr = numberArr.filter(function (value, index) {
        return value >= 0;
    })
    if (soDuongArr.length == 0) {
        document.querySelector("#soDuongNhoNhat").innerHTML = `Không có số dương trong mảng`
    } else {
        var min = soDuongArr[0]
        for (var i = 1; i < soDuongArr.length; i++) {
            if (soDuongArr[i] < min) {
                min = soDuongArr[i];
            }
        }
        document.querySelector("#soDuongNhoNhat").innerHTML = `Số dương nhỏ nhất: ${min}`
    }
}

// Bài 5: Tìm số chẵn cuối cùng
document.querySelector("#timSoChan").onclick = function () {
    var soChan = numberArr[0]
    for (var i = 1; i < numberArr.length; i++) {
        if (numberArr[i] % 2 == 0) {
            soChan = numberArr[i];
        }
    }
    if (soChan % 2 != 0) {
        document.querySelector("#soChanCuoiCung").innerHTML = `Không có số chẵn trong mảng`
    } else {
        document.querySelector("#soChanCuoiCung").innerHTML = `Số chẵn cuối cùng: ${soChan}`
    }
}

//Bài 6: Đổi chỗ
document.querySelector("#doiCho").onclick = function () {
    const nhapSoViTri1 = document.getElementById('nhapSoViTri1').value * 1;
    const nhapSoViTri2 = document.getElementById('nhapSoViTri2').value * 1;
    var swap = numberArr[nhapSoViTri1]
    numberArr[nhapSoViTri1] = numberArr[nhapSoViTri2]
    numberArr[nhapSoViTri2] = swap
    document.getElementById("mangSauKhiDoi").innerHTML = `Mảng sau khi đổi: ${numberArr}`
}

//Bài 7: Sắp xếp tăng dần
document.querySelector("#sapXep").onclick = function () {
    document.getElementById("mangSauKhiSapXep").innerHTML = `Mảng sau khi sắp xếp: ${numberArr.sort()}`
}

//Bài 8: Tìm số nguyên tố đầu tiên
function kiemTraSNT(n) {
    if (n < 2) {
        return false
    }
    else {
        for (i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false
            }
        }
    }
    return true
}

document.querySelector("#timSoNguyenTo").onclick = function () {
    for (var i = 0; i < numberArr.length; i++) {
        var isCoSNT = true
        if (kiemTraSNT(numberArr[i])) {
            document.getElementById("soNguyenToDauTien").innerHTML = `Số nguyên tố đầu tiên: ${numberArr[i]}`
            break
        } else {
            isCoSNT = false
        }
    }
    if (!isCoSNT) {
        document.getElementById("soNguyenToDauTien").innerHTML = `Số nguyên tố đầu tiên: ${-1}`
    }
}

// Bài 9: Đếm số nguyên 
document.querySelector("#themSo2").onclick = function (event) {
    event.preventDefault()
    var nhapSoN2 = document.querySelector("#nhapSoN2").value * 1;
    var mangHienTai2 = document.querySelector("#mangHienTai2")
    numberArr2.push(nhapSoN2)
    document.querySelector("#nhapSoN2").value = ""
    mangHienTai2.innerHTML = `Array: ${numberArr2}`
}

document.querySelector("#demSoNguyen").onclick = function () {
    var countSoNguyen = 0
    numberArr2.forEach(function (item) {
        if (Number.isInteger(item)) {
            countSoNguyen++;
        }
    })
    document.getElementById("soLuongSoNguyen").innerHTML = `Số lượng số nguyên: ${countSoNguyen}`
}

// Bài 10: So sánh số lượng số âm và số dương 
document.querySelector("#soSanh").onclick = function () {
    var countSoDuong = 0
    var countSoAm = 0
    var ketQuaSoSanh =""
    numberArr.forEach(function (item) {
        if (item > 0) {
            countSoDuong++;
        } else {
            countSoAm++;
        }
    })
    if(countSoDuong > countSoAm){
        ketQuaSoSanh = "Số âm < Số dương"
    }else if(countSoDuong < countSoAm){
        ketQuaSoSanh = "Số âm > Số dương"
    }else{
        ketQuaSoSanh = "Số âm = Số dương"
    }
    document.getElementById("ketQuaSoSanh").innerHTML = ketQuaSoSanh
}